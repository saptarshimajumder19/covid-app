import 'package:covid_app/regpage.dart';
import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';


class IntroPage extends StatefulWidget {
  IntroPage({Key key}) : super(key: key);

  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
 
List<Slide> slides = new List();
  @override
  void initState() {
    super.initState();
    slides.add(
   new Slide(
        title: 'Welcome',
        styleTitle: TextStyle(
          color: Colors.blue,
            fontSize: 20.0,
            fontStyle: FontStyle.normal,
            fontFamily: 'Raleway'
        ),
        backgroundColor: Colors.white,
        description:
            "Let's not go by the face",
        styleDescription: TextStyle(
            color: Colors.blue,
            fontSize: 20.0,
            fontStyle: FontStyle.normal,
            fontFamily: 'Raleway'),
        pathImage: "images/covid_avatar.png",
      ),
    );
      slides.add(
     new Slide(
        title: 'Stay home.Stay safe',
        styleTitle: TextStyle(
          color: Colors.blue,
            fontSize: 20.0,
            fontStyle: FontStyle.normal,
            fontFamily: 'Raleway'
        ),
        backgroundColor: Colors.white,
        description:
            "Let us help you with that",
        styleDescription: TextStyle(
            color: Colors.blue,
            fontSize: 20.0,
            fontStyle: FontStyle.normal,
            fontFamily: 'Raleway'),
        pathImage: "images/onboard_page2.jpg",
      ),
  );   
 slides.add(
      new Slide(
        description:"Let's get started",
        styleDescription: TextStyle(
            color: Colors.purpleAccent[400],
            fontSize: 25.0,
            fontStyle: FontStyle.normal,
            fontFamily: 'Raleway'),
        backgroundColor: Colors.white,
        pathImage: "images/onboard_page3.jpg"
      ),
    );
  }

  void onDonePress() {
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => RegPage()));
 }
  @override
 Widget build(BuildContext context) {
    return new IntroSlider(
      colorSkipBtn: Colors.purpleAccent,
      colorDoneBtn: Colors.purpleAccent,
      colorActiveDot: Colors.blue,
      slides: this.slides,
      onDonePress: this.onDonePress,
    );
  }
}